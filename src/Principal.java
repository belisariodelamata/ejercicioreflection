
import co.usuario.tuprograma.bean.Producto;
import java.util.Scanner;

/**
 *
 * @author BELSOFT
 */
public class Principal {

    public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite el número de elementos a almacenar");

        int numeroDeElementos = scanner.nextInt();
        scanner.nextLine();//Consumo del Enter anterior

        Producto[] productos = new Producto[numeroDeElementos];

        for (int i = 0; i < productos.length; i++) {
            Producto producto = new Producto();
            System.out.println("Valor de codigo:");
            producto.setCodigo(scanner.nextLine());
            System.out.println("Valor de Nombre:");
            producto.setNombre(scanner.nextLine());
            System.out.println("Valor de Stock:");
            producto.setStock(scanner.nextInt());
            scanner.nextLine();//Consumo del Enter anterior
            productos[i] = producto;
        }
        System.out.println("----");
        System.out.println("Los Elementos Ingresados son:");
        for (Producto producto : productos) {
            System.out.println(producto);
        }
    }
}
