package co.belisariodelamata.handlersql.util;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 * @author BS
 */
public class BSHandlerSql {

    /**
     * Mapa de Sql
     */
    private transient HashMap<String, String> sqlMap;
    /**
     * Direcccion del Archivo XML
     */
    private transient String xml = "";

    public BSHandlerSql(String rutaPaqueteXml) {
        URL archivoXML = getClass().getResource(rutaPaqueteXml);
        if (archivoXML != null) {
            setXml(archivoXML.getPath());
            examinarXml();
        }
    }

    /**
     * Consulta el SQL desde el HashMap que se encuentra previamente cargado en
     * memoria
     *
     * @param nombreSQL
     * @return
     */
    public String getSQL(String nombreSQL) {
        if (sqlMap != null) {
            return sqlMap.get(nombreSQL);
        } else {
            return getSqlDirect(nombreSQL);
        }
    }

    private void setXml(String xml) {
        this.xml = xml;
    }

    /**
     * Verifica el XML y guarda todas las consultas en un Mapa
     */
    private void examinarXml() {
        sqlMap = new HashMap<>();
        SAXBuilder builder = new SAXBuilder();
        try {
            Document document = builder.build(xml);
            Element root = document.getRootElement();
            for (Element bloque : root.getChildren()) {
                String nombreConsulta = bloque.getAttributeValue("name");
                String sql = bloque.getChildText("sql");
                sqlMap.put(nombreConsulta.toUpperCase(), sql.trim());
            }
        } catch (JDOMException | IOException ex) {
            Logger.getLogger(BSHandlerSql.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Consulta el Sql directamente desde el Archivo XML
     *
     * @param nombreSQL
     * @return
     */
    public String getSqlDirect(String nombreSQL) {
        String sql = null;
        SAXBuilder builder = new SAXBuilder();
        try {
            Document document = builder.build(xml);
            Element root = document.getRootElement();
            for (Element bloque : root.getChildren()) {
                String nombreConsulta = bloque.getAttributeValue("name");
                if (nombreConsulta.trim().toUpperCase().equals(nombreSQL.trim().toUpperCase())) {
                    sql = bloque.getChildText("sql").trim();
                    return sql;
                }
            }
        } catch (JDOMException | IOException ex) {
            Logger.getLogger(BSHandlerSql.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }

}
