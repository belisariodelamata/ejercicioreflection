package co.belisariodelamata.dao;

import co.belisariodelamata.handlersql.util.BSHandlerSql;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author BELSOFT
 */
public abstract class BSParentDAO {

    BSHandlerSql bsHandlerSql;

    public BSParentDAO() {
        iniciarHandlerSql();
    }

    /**
     * Se inicializa el Objeto Manejador del Archivo Xml de las SQL teniendo en
     * cuenta que debe tener el mismo nombre del archivo DAO pero en un
     * subpaquete con nombre xml. Es decir, un archivo DAO con direccion
     * co.ejemplo.dao.ArchivoDAO va a ser mapeado con el archivo
     * co.ejemplo.dao.xml.ArchivoDAO.xml
     */
    void iniciarHandlerSql() {
        String rutaArchivo = "/"
                + (this.getClass().getPackage().getName() + ".xml").replaceAll("\\.", "/")
                + "/" + this.getClass().getSimpleName() + ".xml";
        bsHandlerSql = new BSHandlerSql(rutaArchivo);
    }

    protected String getSQL(String nombreConsulta) {
        return bsHandlerSql.getSQL(nombreConsulta);
    }

    protected Connection conectarBD(String jdbcName) throws SQLException {
        InitialContext initContext;
        Connection cn = null;
        try {
            initContext = new InitialContext();
            DataSource ds = (DataSource) initContext.lookup("java:/comp/env/jdbc/"
                    + jdbcName);
            cn = ds.getConnection();
        } catch (NamingException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        if ((cn == null) || cn.isClosed()) {
            System.out.println("NO HAY CONEXION A LA BASE DE DATOS");
            throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS " + jdbcName);
        }
        return cn;
    }
    protected Connection conectarBDWeb(String jdbcName) throws SQLException {
        InitialContext initContext;
        Connection cn = null;
        try {
            initContext = new InitialContext();
            DataSource ds = (DataSource) initContext.lookup("java:/comp/env/jdbc/"
                    + jdbcName);
            cn = ds.getConnection();
        } catch (NamingException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        if ((cn == null) || cn.isClosed()) {
            System.out.println("NO HAY CONEXION A LA BASE DE DATOS");
            throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS " + jdbcName);
        }
        return cn;
    }

    protected PreparedStatement setParameters(Connection con, String sql, Object... valores)
            throws SQLException {
        PreparedStatement ps = con.prepareStatement(sql);
        for (int i = 0; i < valores.length; i++) {
            if (valores[i] == null) {
                ps.setObject(i + 1, null);
            } else if (valores[i] instanceof String) {
                ps.setString(i + 1, ((String) valores[i]));
            } else if (valores[i] instanceof Float) {
                ps.setFloat(i + 1, ((Float) valores[i]));
            } else if (valores[i] instanceof Double) {
                ps.setDouble(i + 1, ((Double) valores[i]));
            } else {
                ps.setObject(i + 1, (valores[i]));
            }
        }

        return ps;
    }

    protected PreparedStatement setParameters(PreparedStatement ps, Object... valores)
            throws SQLException {
        for (int i = 0; i < valores.length; i++) {
            if (valores[i] == null) {
                ps.setObject(i + 1, null);
            } else if (valores[i] instanceof String) {
                ps.setString(i + 1, ((String) valores[i]));
            } else if (valores[i] instanceof Float) {
                ps.setFloat(i + 1, ((Float) valores[i]));
            } else if (valores[i] instanceof Double) {
                ps.setDouble(i + 1, ((Double) valores[i]));
            } else {
                ps.setObject(i + 1, (valores[i]));
            }
        }

        return ps;
    }

    protected boolean ejecucionSeguraSQL(String jdbcName, List<String> listaSql)
            throws SQLException {
        boolean sucess = false;
        Statement stmt = null;
        Connection con = null;
        con = conectarBD(jdbcName);
        con.setAutoCommit(false);
        try {
            stmt = con.createStatement();
            for (String sentencia : listaSql) {
                if (!(sentencia.toUpperCase().trim().startsWith("SELECT"))) {
                    stmt.addBatch(sentencia);
                }
            }
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            sucess = true;
        } catch (SQLException e) {
            con.rollback();
            throw new SQLException(e.getMessage() + "ERROR : ----> "
                    + e.getNextException());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
            if (con != null) {
                BSParentDAO.cerrar(con);
            }
        }
        return sucess;
    }

    protected static void cerrar(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                logger(BSParentDAO.class, ex);
            }
        }
    }

    protected static void cerrar(ResultSet rs, PreparedStatement ps) {
        cerrar(rs);
        BSParentDAO.cerrar(ps);
    }

    protected static void cerrar(PreparedStatement ps) {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException ex) {
                logger(BSParentDAO.class, ex);
            }
        }
    }

    protected static void cerrar(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                logger(BSParentDAO.class, ex);
            }
        }
    }

    static void logger(Class className, Exception ex) {
        Logger.getLogger(className.getName()).log(Level.SEVERE, null, ex);
    }

    protected void logger(Exception ex) {
        logger(this.getClass(), ex);
    }
}
