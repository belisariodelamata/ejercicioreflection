/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usuario.tuprograma.bean;

/**
 *
 * @author BELSOFT
 */
public class ConfiguracionUnificada {

    private String urlServicio1;
    private String urlServicio2;
    private String urlServicio3;
    private String urlServicio4;
    private int edadMinima;
    private float porcentajeIva;
    private String tokenAcceso;
    private int valorMinimo;
    private long valorMaximo;
    private boolean validarAcceso;

    public String getUrlServicio1() {
        return urlServicio1;
    }

    public void setUrlServicio1(String urlServicio1) {
        this.urlServicio1 = urlServicio1;
    }

    public String getUrlServicio2() {
        return urlServicio2;
    }

    public void setUrlServicio2(String urlServicio2) {
        this.urlServicio2 = urlServicio2;
    }

    public String getUrlServicio3() {
        return urlServicio3;
    }

    public void setUrlServicio3(String urlServicio3) {
        this.urlServicio3 = urlServicio3;
    }

    public String getUrlServicio4() {
        return urlServicio4;
    }

    public void setUrlServicio4(String urlServicio4) {
        this.urlServicio4 = urlServicio4;
    }

    public int getEdadMinima() {
        return edadMinima;
    }

    public void setEdadMinima(int edadMinima) {
        this.edadMinima = edadMinima;
    }

    public float getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(float porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public String getTokenAcceso() {
        return tokenAcceso;
    }

    public void setTokenAcceso(String tokenAcceso) {
        this.tokenAcceso = tokenAcceso;
    }

    public int getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(int valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    public long getValorMaximo() {
        return valorMaximo;
    }

    public void setValorMaximo(long valorMaximo) {
        this.valorMaximo = valorMaximo;
    }

    public boolean isValidarAcceso() {
        return validarAcceso;
    }

    public void setValidarAcceso(boolean validarAcceso) {
        this.validarAcceso = validarAcceso;
    }

}
