/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usuario.tuprograma.dao;

import co.belisariodelamata.dao.BSParentDAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BELSOFT
 */
class ConnectionMariadbDAO extends BSParentDAO {

    protected Connection conectarBD() throws SQLException {
        Connection conexion = null;
        String hostname = "localhost";
        String port = "3306";
        String database = "ws_practica";
        String url = "jdbc:mariadb://" + hostname + ":" + port + "/" + database + "?useSSL=false";
        String user = "root";
        String password = "";
        try {
            conexion = DriverManager.getConnection(url, user, password);
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionMariadbDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conexion;
    }

}
