package co.usuario.tuprograma.dao;

import co.usuario.tuprograma.bean.Configuracion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BELSOFT
 */
public class ConfiguracionDAO extends ConnectionMariadbDAO {

    public List<Configuracion> obtenerConfiguraciones() {
        List<Configuracion> lista = new LinkedList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;
        try (Connection conexion = conectarBD()) {
            //Obtenemos la Conexion y Preparamos la Consulta
            ps = conexion.prepareStatement("select * from configuracion");
            //Ejecutamos la Consulta
            rs = ps.executeQuery();
            //Verificacion si hay registros
            while (rs.next()) {
                //Creamos un Objeto por Cada registro de la consulta
                Configuracion configuracion = new Configuracion();
                configuracion.setPropiedad(rs.getString("propiedad"));
                configuracion.setValor(rs.getString("valor"));
                lista.add(configuracion);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConfiguracionDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cerrar(rs, ps);
        }

        return lista;
    }
}
